# Path: `/usr/local/bin/ltspice_wrapper.sh`

executable_path="$HOME/.wine/dosdevices/c:/Program Files/LTC/LTspiceXVII/XVIIx64.exe"
download_link="http://ltspice.analog.com/software/LTspiceXVII.exe"
tmp_dir=$(mktemp -d) || exit 1

trap 'rm -fr "$tmp_dir"' EXIT

echo $executable_path
if [ -f "$executable_path" ]; then
    wine "$executable_path"
else
    echo "LTSpice not installed. Downloading and running installer..."
    wget $download_link -O $tmp_dir/LTspiceXVII.exe
    chmod +x $tmp_dir/LTspiceXVII.exe
    wine $tmp_dir/LTspiceXVII.exe
fi