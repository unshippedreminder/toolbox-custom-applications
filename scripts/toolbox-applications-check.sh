#!/usr/bin/env bash

####################################
##### Update Toolbox container #####
####################################

# Pull latest image version
podman pull $image # Comment out if using local repo

# Extract newest image ID
newid=$(podman image inspect $image | python3 -c "import sys, json; print(json.load(sys.stdin)[0]['Id'])")

# Check if the newest image ID matches the current one
if [ -z "$(podman inspect applications | grep "$newid")" ]; then
    # Recreate container
    echo "Applications container is outdated. Recreating..."
    toolbox rm -f applications
    toolbox create --container applications --image $image
    echo "Done."
else
    echo "Applications container is up to date."
fi

#############################################
##### Install Applications from Toolbox #####
#############################################

# Install apps from Toolbox
apps='mindforger cura'

# Constants
reporaw='https://gitlab.com/unshippedreminder/toolbox-custom-applications/-/raw/master'
appdir="$HOME/.local/share/applications"
icondir="$HOME/.local/share/icons"

# For each entry in $apps
for i in $apps; do
   echo "Installing: $i"
   # Check if .desktop file already exists
   if [ -f "$appdir/$i.desktop" ]; then
      # If it exists, do nothing
      echo "$appdir/$i.desktop installed. Skipping..."
   else 
      # If it does not exist, download it
      curl $reporaw/applications/$i.desktop --create-dirs --output $appdir/$i.desktop
   fi
   
   # Check if icon file already exists
   if [ -f "$icondir/$i.png" ]; then
      # If it exists, do nothing
      echo "$icondir/$i.png installed. Skipping..."
   else 
      # If it does not exist, download it
      curl $reporaw/icons/$i.png --create-dirs --output $icondir/$i.png
   fi
   # Print a new line for readability
   echo 
done
