echo "Setting up..."
export container_name='fedora-toolbox-working-container'
buildah from registry.fedoraproject.org/fedora-toolbox:34

echo "Building container..."
buildah run $container_name bash -c 'echo "fastestmirror=true" >> /etc/dnf/dnf.conf'
buildah run $container_name dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
                                           https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
buildah run $container_name dnf upgrade -y

buildah copy $container_name ./extra-packages /extra-packages
buildah run $container_name dnf install -y $(<extra-packages)
buildah run $container_name rm /extra-packages 

buildah run $container_name dnf clean all

echo "Configuring container..."
buildah config --label com.github.containers.toolbox="true" \
               --label usage="This image is meant to be used with the toolbox command" \
               --label summary="Personal Toolbox Applications Image" \
               --entrypoint "dnf upgrade -y" $container_name

echo "Finishing up..."
buildah commit $container_name ${CI_PROJECT_NAME}