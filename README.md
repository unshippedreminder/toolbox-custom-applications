# Toolbox Custom Applications

A personal custom image for Toolbox.

## Usage 

### Container creation
To create a toolbox with this image: `toolbox create --container applications --image registry.gitlab.com/unshippedreminder/toolbox-custom-applications` or follow Installation instructions.

### Installation

Move `scripts/toolbox-applications-check.sh` to `~/.local/bin/toolbox-applications-check.sh`
Move `scripts/toolbox-applications-check.service` to `~/.config/systemd/user/toolbox-applications-check.service`

Enable `toolbox-applications-check.service`

One liner (run from repo root): `cp --update scripts/toolbox-applications-check.sh ~/.local/bin/toolbox-applications-check.sh && cp --update scripts/toolbox-applications-check.service ~/.config/systemd/user/toolbox-applications-check.service && systemctl enable --user toolbox-applications-check.service`

### Adding Applications

1. Inside Toolbox: Copy .desktop files from `/usr/share/applications/` or `/usr/local/share/applications/` to `./applications/`
2. Inside Toolbox: Copy icon files from `$XDG_DATA_DIRS/icons` or `/usr/share/pixmaps` to `./icons/`
3. Run `./scripts/toolbox-applications-check.sh` directly or via `./scripts/toolbox-applications-check.service` using the Installation instructions.